start-local:
	echo 'Starting local server'
	docker-compose -f dev-init.yaml --env-file dev-env/.env up --build -d
	echo 'Successfully initiated'

stop-local:
	echo 'Stopping local server'
	docker-compose -f dev-init.yaml --env-file dev-env/.env down
	echo 'Successfully stopped'

