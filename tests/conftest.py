import pytest
import asyncpg
import os
from dotenv import load_dotenv
from motor.motor_asyncio import AsyncIOMotorClient


load_dotenv('../dev-env/.env')
DB_BOOKS_USERNAME = os.environ.get("DB_BOOKS_USERNAME")
DB_BOOKS_PASSWORD = os.environ.get("DB_BOOKS_PASSWORD")
DB_BOOKS_NAME = 'books'
DB_BOOKS_HOST = os.environ.get("DB_BOOKS_HOST")
DB_BOOKS_PORT = os.environ.get("DB_BOOKS_PORT")

DB_USERS_USERNAME = os.environ.get("DB_USERS_ADMIN_USERNAME")
DB_USERS_PASSWORD = os.environ.get("DB_USERS_ADMIN_PASSWORD")
DB_USERS_NAME = 'users'
DB_USERS_HOST = os.environ.get("DB_USERS_HOST")
DB_USERS_PORT = os.environ.get("DB_USERS_PORT")


async def get_books_connection():
    conn = await asyncpg.connect(
        user=DB_BOOKS_USERNAME,
        password=DB_BOOKS_PASSWORD,
        database=DB_BOOKS_NAME,
        host=DB_BOOKS_HOST,
        port=DB_BOOKS_PORT,
    )
    return conn

def get_users_connection():
    connection_string = f'mongodb://{DB_USERS_USERNAME}:{DB_USERS_PASSWORD}@{DB_USERS_HOST}:{DB_USERS_PORT}/{DB_USERS_NAME}'
    print(connection_string)
    return AsyncIOMotorClient(connection_string)[DB_USERS_NAME]

@pytest.fixture(scope="function", autouse=True)
async def clean_db():
    books_conn = await get_books_connection()
    await books_conn.execute(f'DELETE FROM {DB_BOOKS_NAME}')
    await books_conn.close()

    users_conn = get_users_connection()
    users_collection = users_conn.get_collection('users')
    await users_collection.delete_many({})
