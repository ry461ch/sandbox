import pytest
import aiohttp
import asyncio

API_PROXY_URL = 'http://0.0.0.0:80'


@pytest.mark.asyncio
async def test_get_books(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        response = await session.get(API_PROXY_URL + '/books')

        assert response.status == 200
        assert await response.json() == {'books': []}
        assert response.headers['content-type'] == 'application/json'


@pytest.mark.asyncio
async def test_add_book(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        await session.post(API_PROXY_URL + '/books/add', json={'author': 'tester', 'name': 'book_test'})
        response = await session.get(API_PROXY_URL + '/books')
        assert response.status == 200
        response_data = await response.json()

        assert len(response_data['books']) == 1
        assert response_data['books'][0]['author'] == 'tester'
        assert response_data['books'][0]['name'] == 'book_test'
        assert response.headers['content-type'] == 'application/json'


@pytest.mark.asyncio
async def test_get_book(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        await session.post(API_PROXY_URL + '/books/add', json={'author': 'tester', 'name': 'book_test'})
        response = await session.get(API_PROXY_URL + '/books')
        assert response.status == 200
        response_data = await response.json()

        assert len(response_data['books']) == 1
        response = await session.get(API_PROXY_URL + f'/books/{response_data["books"][0]["id"]}')
        response_data = await response.json()
        assert response_data['author'] == 'tester'
        assert response_data['name'] == 'book_test'
        assert response.headers['content-type'] == 'application/json'
