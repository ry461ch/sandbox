import pytest
import aiohttp
import asyncio

API_PROXY_URL = 'http://0.0.0.0:80'


@pytest.mark.asyncio
async def test_get_users(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        response = await session.get(API_PROXY_URL + '/users')

        assert response.status == 200
        assert await response.json() == {'users': []}


@pytest.mark.asyncio
async def test_add_user(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        await session.post(API_PROXY_URL + '/users/add', json={'login': 'tester', 'phone': 'phone_test'})
        response = await session.get(API_PROXY_URL + '/users')
        assert response.status == 200
        response_data = await response.json()

        assert len(response_data['users']) == 1
        assert response_data['users'][0]['login'] == 'tester'
        assert response_data['users'][0]['phone'] == 'phone_test'


@pytest.mark.asyncio
async def test_get_book(clean_db):
    await clean_db
    async with aiohttp.ClientSession() as session:
        await session.post(API_PROXY_URL + '/users/add', json={'login': 'tester', 'phone': 'phone_test', 'address': {'city': 'test_city'}})
        response = await session.get(API_PROXY_URL + '/users')
        assert response.status == 200
        response_data = await response.json()

        assert len(response_data['users']) == 1
        response = await session.get(API_PROXY_URL + f'/users/{response_data["users"][0]["_id"]}')
        response_data = await response.json()

        assert response_data['login'] == 'tester'
        assert response_data['phone'] == 'phone_test'
        assert response_data['address'] == {'city': 'test_city'}
