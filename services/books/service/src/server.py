from flask import Flask, Response, request, jsonify
import asyncpg
import asyncio
import os
from dotenv import load_dotenv

load_dotenv('/env/.env')
DB_USERNAME = os.environ.get("DB_BOOKS_USERNAME")
DB_PASSWORD = os.environ.get("DB_BOOKS_PASSWORD")
DB_NAME = 'books'
DB_HOST = os.environ.get("DB_BOOKS_CONTAINER_NAME")

app = Flask(__name__)


@app.route('/books', methods = ['GET'])
async def get_books():
    print('get books')
    conn = await asyncpg.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        database=DB_NAME,
        host=DB_HOST,
        port=5432
    )
    books = await conn.fetch('SELECT * FROM books')
    await conn.close()
    return jsonify({"books": [
        {
            'name': record['name'],
            'id': record['id'],
            'author': record['author']
        } for record in books
    ]})

@app.route('/books/add', methods = ['POST'])
async def add_book():
    print('add book')
    book = request.json
    conn = await asyncpg.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        database=DB_NAME,
        host=DB_HOST,
        port=5432
    )
    await conn.execute(
        'INSERT INTO books (name, author) values ($1, $2)',
        book['name'],
        book['author']
    )
    await conn.close()
    return Response(status=201)

@app.route('/books/<id>', methods = ['GET'])
async def get_book(id):
    print('get book')
    print(DB_HOST, DB_NAME, DB_PASSWORD, DB_USERNAME)
    conn = await asyncpg.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        database=DB_NAME,
        host=DB_HOST,
        port=5432
    )
    record = await conn.fetch('SELECT * FROM books WHERE id = $1', int(id))
    await conn.close()
    return jsonify(
        {
            'name': record[0]['name'],
            'id': record[0]['id'],
            'author': record[0]['author']
        }
    )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(app.run(host="0.0.0.0", port=3000))
