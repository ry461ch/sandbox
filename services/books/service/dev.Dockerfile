FROM ubuntu:22.04
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

ENV SERVICE_DIR /service
WORKDIR ${SERVICE_DIR}
RUN apt-get update
RUN apt-get -y install curl
RUN apt-get -y install python3-pip

COPY ./src ./src
COPY ./requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt

EXPOSE "3000/tcp"
ENTRYPOINT [ "python3", "src/server.py" ]
