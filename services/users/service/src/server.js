const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const env = dotenv.config({ path: '/env/.env' });
const DB_NAME = 'users';
const app = express();
const port = 3000;
app.use(express.json());

const url = `mongodb://${process.env.DB_USERS_ADMIN_USERNAME}:${process.env.DB_USERS_ADMIN_PASSWORD}@${process.env.DB_USERS_CONTAINER_NAME}:27017/${DB_NAME}`;
mongoose.connect(url);

const db = mongoose.connection;
db.once('open', _ => {
  console.log('Database connection established at', url)
})

db.on('error', err => {
  console.error('Database connection error due to:', err)
})

const userSchema = new mongoose.Schema({
    login: { type: String, required: true, index: { unique: true }},
    phone: { type: String, required: true },
    address: {
      city: String,
      zip: String
    }
});
const User = mongoose.model('User', userSchema);

app.get('/users', async (req, res) => {
    users = await User.find({});
    res.send({ users });
})

app.get('/users/:id', async (req, res) => {
    user = await User.findById(req.params.id);
    res.send(user);
})

app.post('/users/add', async (req, res) => {
    const user = new User({
        login: req.body.login,
        phone: req.body.phone,
        address: {
            city: req.body.address ? req.body.address.city : null,
            zip: req.body.address ? req.body.address.zip : null
        }
    });
    await user.save();
    res.sendStatus(200);
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})
