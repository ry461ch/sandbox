FROM ubuntu:22.04
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

ENV SERVICE_DIR /service
RUN mkdir -p ${SERVICE_DIR}/.nvm
WORKDIR ${SERVICE_DIR}
RUN apt-get update
RUN apt-get install -y curl

ENV NVM_DIR ${SERVICE_DIR}/.nvm
ENV NODE_VERSION 18.18.2
COPY ./src ./src
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install ${NODE_VERSION} \
    && nvm alias default ${NODE_VERSION} \
    && nvm use default \
    && npm install
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

EXPOSE 3000/tcp
ENTRYPOINT [ "npm", "run", "start-server" ]
