db = db.getSiblingDB('users');
db.createUser(
    {
        user: process.env.MONGO_ADMIN_USERNAME,
        pwd: process.env.MONGO_ADMIN_PASSWORD,
        roles: [
            {
                role: "readWrite",
                db: "users"
            }
        ]
    }
);
